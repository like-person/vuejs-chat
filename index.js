var app = new Vue({ 
    el: '#app',
    data: {
        message: 'Hello Vue!',
		userName: 'Damir',
		userAvatar: 'https://lk.mozgokachka.ru/upload/resize_cache/main/40b/110_110_2/40bb3a7918e0c9337c46db1798a24950.png',
		userAuthorized: false,
		users: [
			{ 
				id: 0, 
				name: 'Иванов Иван', 
				avatar: 'https://lk.mozgokachka.ru/upload/iblock/d52/d52118f1b1d64b084d77608b848cc8c7.png',
				contacts: [
					{id: 0, name: 'Иванов Иван'}
				],
				selectedContact: 1,
				curMessage: ''
			},
		],
		messages: [
		  { id: 'msg0', author: 0, to: 1, datetime: 1595367227, message: 'Hello' },
		  { id: 'msg1', author: 1, to: 0, datetime: 1595367227, message: 'Hi' }
		],
    },
	methods: {
		onSign: function () {
			this.userAuthorized = true;
			let i = this.users.length;
			this.users.push({ 
				id: i, 
				name: this.userName,
				avatar: this.userAvatar,
				contacts: [
					{id: 0, name: 'Иванов Иван'}
				],
				selectedContact: 0,
				curMessage: ''
			});
			this.users[0].contacts.push({id: i, name: this.userName});
			this.users[i].contacts.push({id: i, name: this.userName});
		},
		onSignout: function () {
			this.userAuthorized = false;
			this.userName = '';
			this.userAvatar = '';
		},
		send: function (user) {
			if(user.curMessage != '') {
				let i = this.messages.length;
				this.messages.push({id: 'msg' + i, author: user.id, to: user.selectedContact, datetime: 1595367227, message: user.curMessage});
				user.curMessage = '';
			} else {
				alert('Empty message')
			}
			this.$refs["sendMessage" + user.id][0].focus()
		}
	}
});